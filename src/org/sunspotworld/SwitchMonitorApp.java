/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sunspotworld;

	

    import com.sun.spot.io.j2me.radiogram.RadiogramConnection;
    import com.sun.spot.peripheral.ota.OTACommandServer;
    import java.text.SimpleDateFormat;
    import javax.microedition.io.Connector;
    import javax.microedition.io.Datagram;
     
    /**
     *
     * @author Babblebase
     */
    public class SwitchMonitorApp implements Runnable
    {
        //Broadcasr port
        private static final int HOST_PORT = 67;
       
        //connection and datagram variables
        private RadiogramConnection radioConn;
        private Datagram datagram;
       
        //thread used for communicatiing with spot
        private Thread pollingThread = null;
       
        public SwitchMonitorApp()
        {
            try
            {
                //start polling thread
                startPolling();
            }
            catch(Exception e)
            {
                System.out.println("Polling failed to initialise");
            }
        }
        /**
         * innitialise thread for communication with other spot devices
         * @throws Exception
         */
        public void startPolling() throws Exception
        {
            pollingThread = new Thread(this, "pollingService");
            pollingThread.setDaemon(true);
            pollingThread.start();
        }
       
        /**
         * start host computer application
         */
        public void run() {
            try
            {
                //opens a server side radio broadcast radiogram connection
                //to listen for switch readings being sent by the spot device
                radioConn = (RadiogramConnection) Connector.open("radiogram://:"
                        + HOST_PORT);
                datagram = radioConn.newDatagram(radioConn.getMaximumLength());
                System.out.println("RADIO STARTED");
            }
            catch(Exception e)
            {
                System.err.println("setUp caught" + e.getMessage());
            }
           
            while(true)
            {
                try
                {
                    //reads switch state data sent over the radio
                    radioConn.receive(datagram);
                    //read sensor ID
                    String addr = datagram.getAddress();
                    //reads time of switch 'press' reading
                    long time = datagram.readLong();
                    String switchID = datagram.readUTF();
                    

                   
                    //formate time from device
                    SimpleDateFormat dateFormatter = new SimpleDateFormat("E "
                            + "dd/MM/yyyy '@'hh:mm:ss a zzz");
                    //prints the id of switch pressed, the senders id
                    //and time of transmision
                    System.out.println("Switch pressed: " +switchID+" Spot ID: "
                    + addr + "time" + dateFormatter.format(time));
                }
                catch(Exception e)
                {
                    System.out.println("Caught " + e + " while polling SPOT");
                }
            }
        }
       
        public static void main(String[] args) throws Exception
        {
            //register application name with the OTA command server and startt serv
            OTACommandServer.start("SwitchMonitorApplication");
            SwitchMonitorApp SwitchMonitorApp = new SwitchMonitorApp();
        }
       
    }


